package net.jmodwyer.beacon.beaconPoC;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import java.util.Collection;
import java.util.HashMap;

import net.jmodwyer.ibeacon.ibeaconPoC.R;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;

public class ScanActivity extends Activity implements BeaconConsumer {
    private Animation fadeIn;
    private Animation fadeOut;
    private BeaconManager beaconManager;
    private boolean already_shown = true;
    private Region region;
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 100;
    private static final String PREFERENCE_SCANINTERVAL = "scanInterval";
    private ImageView iv;
    private ImageView iv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        if (getActionBar() != null)
            getActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.banniere));
        verifyBluetooth();
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        BeaconScannerApp app = (BeaconScannerApp) this.getApplication();
        beaconManager = app.getBeaconManager();
        region = app.getRegion();
        iv = (ImageView) ScanActivity.this.findViewById(R.id.background);
        iv2 = (ImageView) ScanActivity.this.findViewById(R.id.background2);
        beaconManager.bind(this);
        fadeIn = AnimationUtils.loadAnimation(ScanActivity.this, R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(ScanActivity.this, R.anim.fade_out);
    }

    @Override
    public void onResume() {
        super.onResume();
        beaconManager.bind(this);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onBeaconServiceConnect() {
        startScanning();
    }

	private void startScanning() {
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
	    HashMap <String, Object> prefs = new HashMap<String, Object>();
	    prefs.putAll(sharedPrefs.getAll());
		if (prefs.get(PREFERENCE_SCANINTERVAL) != null) {
            beaconManager.setBackgroundBetweenScanPeriod(10L);
            beaconManager.setForegroundBetweenScanPeriod(10L);
		}
        beaconManager.setRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                if (beacons.size() > 0) {
                    for (Beacon beacon : beacons) {
                        if (beacon.getDistance() < 0.5)
                            logGenericBeacon(beacon);
                        else if (!already_shown) {
                            already_shown = true;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    iv2.startAnimation(fadeOut);
                                    fadeOut.setAnimationListener(new Animation.AnimationListener() {
                                        @Override
                                        public void onAnimationStart(Animation animation) {
                                        }
                                        @Override
                                        public void onAnimationEnd(Animation animation) {
                                            iv2.setVisibility(View.GONE);
                                            iv.setVisibility(View.VISIBLE);
                                            iv.startAnimation(fadeIn);
                                        }
                                        @Override
                                        public void onAnimationRepeat(Animation animation) {
                                        }
                                    });
                                }
                            });
                        }
                    }
                }
            }
        });
        try {
            beaconManager.startRangingBeaconsInRegion(region);
        } catch (RemoteException e) {
            //
        }
	}

    private void logGenericBeacon(Beacon beacon) {
        if (beacon.getServiceUuid() != 0xfeaa) {
            if (beacon.getId1().toString().equals("ab8190d5-d11e-4941-acc4-42f30510b408"))
            {
                if (already_shown) {
                    already_shown = false;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            iv.startAnimation(fadeOut);
                            fadeOut.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {
                                }
                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    iv.setVisibility(View.GONE);
                                    iv2.setVisibility(View.VISIBLE);
                                    iv2.startAnimation(fadeIn);
                                }
                                @Override
                                public void onAnimationRepeat(Animation animation) {
                                }
                            });
                        }
                    });
                }
            }
        }
    }

 	private void verifyBluetooth() {
		try {
			if (!BeaconManager.getInstanceForApplication(this).checkAvailability()) {
				final AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Bluetooth désactivé");
				builder.setMessage("Activez le bluetooth dans vos paramètres, puis redémarrez votre application.");
				builder.setPositiveButton(android.R.string.ok, null);
				builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						finish();
			            System.exit(0);					
					}					
				});
				builder.show();
			}			
		}
		catch (RuntimeException e) {
			final AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Bluetooth non disponible");
			builder.setMessage("Votre équipement ne prends pas en charge le bluetooth.");
			builder.setPositiveButton(android.R.string.ok, null);
			builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					finish();
		            System.exit(0);					
				}
			});
			builder.show();
		}
	}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST :
                switch (resultCode) {
                    case Activity.RESULT_OK :
                        break;
                }
        }
    }
}