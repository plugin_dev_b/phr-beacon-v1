package net.jmodwyer.beacon.beaconPoC;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import net.jmodwyer.ibeacon.ibeaconPoC.R;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Handles the listing of files and allows the user to select files for deletion and/or e-mailing.
 * @author justin
 *
 */
public class FileHandlerActivity extends Activity {
	
	private FileHelper fileHelper;
	private ArrayList<String> list;
	private static String selectedItem;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
	    super.onCreate(savedInstanceState);
	    
	    // Get the file helper and list files.
		BeaconScannerApp app = (BeaconScannerApp)this.getApplication();
		fileHelper = app.getFileHelper();
	    
	    setContentView(R.layout.activity_filelist);
	    getActionBar().setDisplayHomeAsUpEnabled(true);
	    
	    refreshFileList();

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    return true;
	}
	
	// Handle the user selecting items from the action bar.
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	/**
	 * Refresh the list of files so the current state is always displayed. 
	 * We call this when intialising the activity, and also after deleting a file.  
	 */
	private void refreshFileList() {
	    ListView listView = (ListView) findViewById(R.id.listview);
	    list = fileHelper.listFiles();
	    listView.setAdapter(new ArrayAdapter<String>(this, R.layout.file_list_item, list));
	    listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);  
	 
	    listView.setOnItemClickListener(new OnItemClickListener() {
		   @Override
		   public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
              selectedItem = ((TextView)arg1).getText().toString();
		   } 
		});
	}
	
}
